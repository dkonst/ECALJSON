How to use scripts:

***
bash GetLumisectionsJSON.sh run1 run2 run3 ... runN
to produce JSON file with lumisections intervals.
Empty runs are skipped automatically, script should be called from lxplus

***
python mergeJSON.py jsonfile1 jsonfile2
gives two JSON files merged in standart output