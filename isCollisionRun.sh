#!/bin/bash

# The script prints to stdout JSON data for given runs which contains continuous intervals of lumisection in which ECAL is GREEN and beam is Stable.
# Script requires valide Kerberos ticket and should be run from lxplus.cern.ch (or other node with cern-get-sso-cookie package installed)
#
# Author: Latyshev Grigory
# Date: 07/07/2015

tmpdir=$(mktemp -d)

if ! klist &> /dev/null; then
  kdestroy
  kinit -k -t /usr/local/user.keytab glatyshe@CERN.CH
  deleteme=1
else
  deleteme=0
fi

cern-get-sso-cookie --krb -r -u "https://cmswbm.web.cern.ch" -o $tmpdir/ssocookie.txt

chmod 600 $tmpdir/ssocookie.txt

runs="$@"
if [ -z "$runs" -o "$1" == "-h" -o "$1" == "--help" ];then
  echo "Usage: bash {0} <run> <run> <run> ... <run>"
  exit 0
fi

test $deleteme -eq 1 && kdestroy

# python analyser
cat <<EOF > $tmpdir/analyser.py
#!/usr/bin/env python

import sys
import os
import re
from operator import itemgetter
from itertools import groupby

lumire = re.compile("<TD.*LUMISECTION=(.+)>.+</A>")
condre = lambda x: re.compile(".*TITLE=\"{0}\" BGCOLOR=GREEN.*".format(x))

def getLumisList(text):
  lumis = []
  for l in text.split('\n'):
    l = l.strip()
    if lumire.match(l):
      lumi = int(lumire.findall(l)[0])
      continue
    if  condre("beam1Present").match(l) and condre("beam2Present").match(l) \
    and condre("beam1Stable").match(l) and condre("beam2Stable").match(l) \
    and condre("Run Active").match(l) and condre("physics").match(l):
      lumis.append(lumi)
    else:
      continue
  return lumis

a = getLumisList(sys.stdin.read())
if len(a) > 1:
  sys.exit(0)
else:
  sys.exit(1)

EOF

for r in $(echo $runs | tr ' ' '\n'); do 
  curl -s -L --cookie $tmpdir/ssocookie.txt --cookie-jar $tmpdir/ssocookie.txt https://cmswbm.web.cern.ch/cmswbm/cmsdb/servlet/LumiSections?RUN=$r | python $tmpdir/analyser.py && echo $r
done

rm -rf $tmpdir
