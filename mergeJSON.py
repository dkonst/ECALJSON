#!/usr/bin/env python

import sys
import json

def drepr(x, sort = True, indent = 0, shift = 2):
  if isinstance(x, dict):
    r = '{\n'
    for (key, value) in (sorted(x.items()) if sort else x.iteritems()):
      r += (' ' * (indent + shift)) + repr(key) + ': '
      r += drepr(value, sort, indent + shift) + ',\n'
    r = r.rstrip(',\n') + '\n'
    r += (' ' * indent) + '}'
  elif hasattr(x, '__iter__'):
    r = '[\n'
    for value in (sorted(x) if sort else x):
      r += (' ' * (indent + shift)) + drepr(value, sort, indent + shift) + ',\n'
    r = r.rstrip(',\n') + '\n'
    r += (' ' * indent) + ']'
  else:
    r = repr(x)
  return r


files = sys.argv[1:]

result = {}

for f in files:
    tmp = json.loads(open(f, 'r').read())
    todel = []
    for k in tmp.iterkeys():
      if result.has_key(k):
        todel.append(k)
    for i in todel:
      del tmp[i]
    result.update(tmp)

print drepr(result)
